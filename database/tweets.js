module.exports = [
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1583513381222645761"
          ],
          "editableUntil" : "2022-10-21T18:09:44.000Z",
          "editsRemaining" : "5",
          "isEditEligible" : false
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "0",
              "13"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "66"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1583512932734095362",
      "id_str" : "1583513381222645761",
      "in_reply_to_user_id" : "1096775695957602304",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1583513381222645761",
      "in_reply_to_status_id" : "1583512932734095362",
      "created_at" : "Fri Oct 21 17:39:44 +0000 2022",
      "favorited" : false,
      "full_text" : "@reinhart1010 OH SHIFT DOES THAT MEAN I'M HAVING AN i++ RIGHT NOW?",
      "lang" : "en",
      "in_reply_to_screen_name" : "capsinthehouse",
      "in_reply_to_user_id_str" : "1096775695957602304"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1583512932734095362"
          ],
          "editableUntil" : "2022-10-21T18:07:58.000Z",
          "editsRemaining" : "5",
          "isEditEligible" : false
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "0",
              "13"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "145"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1583512229600956416",
      "id_str" : "1583512932734095362",
      "in_reply_to_user_id" : "3291665198",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1583512932734095362",
      "in_reply_to_status_id" : "1583512229600956416",
      "created_at" : "Fri Oct 21 17:37:58 +0000 2022",
      "favorited" : false,
      "full_text" : "@reinhart1010 YES. IT'S FINALLY THE DAY I'M GETTING INTO THE HOUSE FOR TERMINATING FIRES WITH ASYNC FUNCTIONS AND PARTITIONING MY BIRTHDAY CAKES.",
      "lang" : "en",
      "in_reply_to_screen_name" : "reinhart1010",
      "in_reply_to_user_id_str" : "3291665198"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1574372025589956608"
          ],
          "editableUntil" : "2022-09-26T12:45:15.000Z",
          "editsRemaining" : "5",
          "isEditEligible" : false
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Rizki Salminen 💜",
            "screen_name" : "tilehopper",
            "indices" : [
              "0",
              "11"
            ],
            "id_str" : "1249662133916192768",
            "id" : "1249662133916192768"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/sH6lyGh8S3",
            "expanded_url" : "https://twitter.com/reinhart1010/status/1574364426463227904?t=zGPbJMb35QLaDVQMwL2Nqg&s=19",
            "display_url" : "twitter.com/reinhart1010/s…",
            "indices" : [
              "12",
              "35"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "35"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1574194931266035718",
      "id_str" : "1574372025589956608",
      "in_reply_to_user_id" : "1249662133916192768",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1574372025589956608",
      "in_reply_to_status_id" : "1574194931266035718",
      "possibly_sensitive" : false,
      "created_at" : "Mon Sep 26 12:15:15 +0000 2022",
      "favorited" : false,
      "full_text" : "@tilehopper https://t.co/sH6lyGh8S3",
      "lang" : "qme",
      "in_reply_to_screen_name" : "tilehopper",
      "in_reply_to_user_id_str" : "1249662133916192768"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1560889271183351809"
          ],
          "editableUntil" : "2022-08-20T07:49:36.000Z",
          "editsRemaining" : "5",
          "isEditEligible" : false
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [ ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1560889271183351809/photo/1",
            "indices" : [
              "110",
              "133"
            ],
            "url" : "https://t.co/bAHyfd7O8q",
            "media_url" : "http://pbs.twimg.com/media/FalkgWIacAEfmRx.jpg",
            "id_str" : "1560889252720046081",
            "id" : "1560889252720046081",
            "media_url_https" : "https://pbs.twimg.com/media/FalkgWIacAEfmRx.jpg",
            "sizes" : {
              "large" : {
                "w" : "922",
                "h" : "2048",
                "resize" : "fit"
              },
              "small" : {
                "w" : "306",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "540",
                "h" : "1200",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/bAHyfd7O8q"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "133"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1560887623564283905",
      "id_str" : "1560889271183351809",
      "in_reply_to_user_id" : "1096775695957602304",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1560889271183351809",
      "in_reply_to_status_id" : "1560887623564283905",
      "possibly_sensitive" : false,
      "created_at" : "Sat Aug 20 07:19:36 +0000 2022",
      "favorited" : false,
      "full_text" : "Email tersebut dianggap sebagai \"keluhan\", bukan \"pertanyaan umum\" oleh tim CS aplikasi yang mengiklankannya. https://t.co/bAHyfd7O8q",
      "lang" : "in",
      "in_reply_to_screen_name" : "capsinthehouse",
      "in_reply_to_user_id_str" : "1096775695957602304",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1560889271183351809/photo/1",
            "indices" : [
              "110",
              "133"
            ],
            "url" : "https://t.co/bAHyfd7O8q",
            "media_url" : "http://pbs.twimg.com/media/FalkgWIacAEfmRx.jpg",
            "id_str" : "1560889252720046081",
            "id" : "1560889252720046081",
            "media_url_https" : "https://pbs.twimg.com/media/FalkgWIacAEfmRx.jpg",
            "sizes" : {
              "large" : {
                "w" : "922",
                "h" : "2048",
                "resize" : "fit"
              },
              "small" : {
                "w" : "306",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "540",
                "h" : "1200",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/bAHyfd7O8q"
          },
          {
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1560889271183351809/photo/1",
            "indices" : [
              "110",
              "133"
            ],
            "url" : "https://t.co/bAHyfd7O8q",
            "media_url" : "http://pbs.twimg.com/media/FalkhHhaIAIw9C-.jpg",
            "id_str" : "1560889265978220546",
            "id" : "1560889265978220546",
            "media_url_https" : "https://pbs.twimg.com/media/FalkhHhaIAIw9C-.jpg",
            "sizes" : {
              "medium" : {
                "w" : "540",
                "h" : "1200",
                "resize" : "fit"
              },
              "small" : {
                "w" : "306",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "922",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/bAHyfd7O8q"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1560887623564283905"
          ],
          "editableUntil" : "2022-08-20T07:43:04.000Z",
          "editsRemaining" : "5",
          "isEditEligible" : false
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [ ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1560887623564283905/photo/1",
            "indices" : [
              "59",
              "82"
            ],
            "url" : "https://t.co/6dlQtxeXLr",
            "media_url" : "http://pbs.twimg.com/media/FaljARHacAA-YsR.jpg",
            "id_str" : "1560887602106232832",
            "id" : "1560887602106232832",
            "media_url_https" : "https://pbs.twimg.com/media/FaljARHacAA-YsR.jpg",
            "sizes" : {
              "medium" : {
                "w" : "540",
                "h" : "1200",
                "resize" : "fit"
              },
              "small" : {
                "w" : "306",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "922",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/6dlQtxeXLr"
          }
        ],
        "hashtags" : [
          {
            "text" : "NgiklanYangBenerDong",
            "indices" : [
              "37",
              "58"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "82"
      ],
      "favorite_count" : "0",
      "id_str" : "1560887623564283905",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1560887623564283905",
      "possibly_sensitive" : false,
      "created_at" : "Sat Aug 20 07:13:04 +0000 2022",
      "favorited" : false,
      "full_text" : "How it all started, how is it going. #NgiklanYangBenerDong https://t.co/6dlQtxeXLr",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1560887623564283905/photo/1",
            "indices" : [
              "59",
              "82"
            ],
            "url" : "https://t.co/6dlQtxeXLr",
            "media_url" : "http://pbs.twimg.com/media/FaljARHacAA-YsR.jpg",
            "id_str" : "1560887602106232832",
            "id" : "1560887602106232832",
            "media_url_https" : "https://pbs.twimg.com/media/FaljARHacAA-YsR.jpg",
            "sizes" : {
              "medium" : {
                "w" : "540",
                "h" : "1200",
                "resize" : "fit"
              },
              "small" : {
                "w" : "306",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "922",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/6dlQtxeXLr"
          },
          {
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1560887623564283905/photo/1",
            "indices" : [
              "59",
              "82"
            ],
            "url" : "https://t.co/6dlQtxeXLr",
            "media_url" : "http://pbs.twimg.com/media/FaljA2facAE6tmr.jpg",
            "id_str" : "1560887612139008001",
            "id" : "1560887612139008001",
            "media_url_https" : "https://pbs.twimg.com/media/FaljA2facAE6tmr.jpg",
            "sizes" : {
              "small" : {
                "w" : "306",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "922",
                "h" : "2048",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "540",
                "h" : "1200",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/6dlQtxeXLr"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1553386989680218112"
          ],
          "editableUntil" : "2022-07-30T14:58:13.000Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "3",
              "16"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1553386989680218112/photo/1",
            "indices" : [
              "18",
              "41"
            ],
            "url" : "https://t.co/lKfMuZ9VRs",
            "media_url" : "http://pbs.twimg.com/media/FY69OuCaUAA3Cbp.jpg",
            "id_str" : "1553386982063362048",
            "id" : "1553386982063362048",
            "media_url_https" : "https://pbs.twimg.com/media/FY69OuCaUAA3Cbp.jpg",
            "sizes" : {
              "large" : {
                "w" : "900",
                "h" : "900",
                "resize" : "fit"
              },
              "small" : {
                "w" : "680",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "900",
                "h" : "900",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/lKfMuZ9VRs"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "41"
      ],
      "favorite_count" : "0",
      "id_str" : "1553386989680218112",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1553386989680218112",
      "possibly_sensitive" : false,
      "created_at" : "Sat Jul 30 14:28:13 +0000 2022",
      "favorited" : false,
      "full_text" : "Hi @reinhart1010! https://t.co/lKfMuZ9VRs",
      "lang" : "und",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1553386989680218112/photo/1",
            "indices" : [
              "18",
              "41"
            ],
            "url" : "https://t.co/lKfMuZ9VRs",
            "media_url" : "http://pbs.twimg.com/media/FY69OuCaUAA3Cbp.jpg",
            "id_str" : "1553386982063362048",
            "id" : "1553386982063362048",
            "media_url_https" : "https://pbs.twimg.com/media/FY69OuCaUAA3Cbp.jpg",
            "sizes" : {
              "large" : {
                "w" : "900",
                "h" : "900",
                "resize" : "fit"
              },
              "small" : {
                "w" : "680",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "900",
                "h" : "900",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/lKfMuZ9VRs"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1553385993193291776"
          ],
          "editableUntil" : "2022-07-30T14:54:15.950Z",
          "editsRemaining" : "5",
          "isEditEligible" : false
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Imre Nagi",
            "screen_name" : "imrenagi",
            "indices" : [
              "3",
              "12"
            ],
            "id_str" : "157604142",
            "id" : "157604142"
          },
          {
            "name" : "Kementerian Kominfo",
            "screen_name" : "kemkominfo",
            "indices" : [
              "95",
              "106"
            ],
            "id_str" : "177848697",
            "id" : "177848697"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "136"
      ],
      "favorite_count" : "0",
      "id_str" : "1553385993193291776",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1553385993193291776",
      "created_at" : "Sat Jul 30 14:24:15 +0000 2022",
      "favorited" : false,
      "full_text" : "RT @imrenagi: Hey kalian anak bangsa, kalau platform game diblokir ya kalian buat sendiri lah. @kemkominfo yakin anak bangsa pasti bisa!",
      "lang" : "in"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1414207875895861248"
          ],
          "editableUntil" : "2021-07-11T13:30:06.982Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "0",
              "8"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "38"
      ],
      "favorite_count" : "1",
      "in_reply_to_status_id_str" : "1413526285398118403",
      "id_str" : "1414207875895861248",
      "in_reply_to_user_id" : "2142731",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1414207875895861248",
      "in_reply_to_status_id" : "1413526285398118403",
      "created_at" : "Sun Jul 11 13:00:06 +0000 2021",
      "favorited" : false,
      "full_text" : "@firefox Is there a shortcut for that?",
      "lang" : "en",
      "in_reply_to_screen_name" : "firefox",
      "in_reply_to_user_id_str" : "2142731"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1317433734057000966"
          ],
          "editableUntil" : "2020-10-17T12:24:13.591Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "TANYAOCBCNISP",
            "screen_name" : "TanyaOCBCNISP",
            "indices" : [
              "6",
              "20"
            ],
            "id_str" : "1108330727827869696",
            "id" : "1108330727827869696"
          },
          {
            "name" : "DANA.id",
            "screen_name" : "danawallet",
            "indices" : [
              "21",
              "32"
            ],
            "id_str" : "965871936545816577",
            "id" : "965871936545816577"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1317433734057000966/photo/1",
            "indices" : [
              "281",
              "304"
            ],
            "url" : "https://t.co/9x1iUOeYgD",
            "media_url" : "http://pbs.twimg.com/media/Ekh2_fhVMAEKQEQ.jpg",
            "id_str" : "1317433724171005953",
            "id" : "1317433724171005953",
            "media_url_https" : "https://pbs.twimg.com/media/Ekh2_fhVMAEKQEQ.jpg",
            "sizes" : {
              "large" : {
                "w" : "828",
                "h" : "1792",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "554",
                "h" : "1200",
                "resize" : "fit"
              },
              "small" : {
                "w" : "314",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/9x1iUOeYgD"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "304"
      ],
      "favorite_count" : "1",
      "id_str" : "1317433734057000966",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1317433734057000966",
      "possibly_sensitive" : false,
      "created_at" : "Sat Oct 17 11:54:13 +0000 2020",
      "favorited" : false,
      "full_text" : "Malam @TanyaOCBCNISP @danawallet saya ingin menambahkan kartu debit OCBC NISP (baik kartu fisik Mastercard maupun online) di aplikasi DANA, namun kedua kartu tersebut seringkali ditolak. Apakah saya harus mengaktifkannya untuk transaksi online secara manual dan bagaimana caranya? https://t.co/9x1iUOeYgD",
      "lang" : "in",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1317433734057000966/photo/1",
            "indices" : [
              "281",
              "304"
            ],
            "url" : "https://t.co/9x1iUOeYgD",
            "media_url" : "http://pbs.twimg.com/media/Ekh2_fhVMAEKQEQ.jpg",
            "id_str" : "1317433724171005953",
            "id" : "1317433724171005953",
            "media_url_https" : "https://pbs.twimg.com/media/Ekh2_fhVMAEKQEQ.jpg",
            "sizes" : {
              "large" : {
                "w" : "828",
                "h" : "1792",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "554",
                "h" : "1200",
                "resize" : "fit"
              },
              "small" : {
                "w" : "314",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/9x1iUOeYgD"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1281966856882974721"
          ],
          "editableUntil" : "2020-07-11T15:31:31.130Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Taylor Otwell 🪐",
            "screen_name" : "taylorotwell",
            "indices" : [
              "3",
              "16"
            ],
            "id_str" : "28870687",
            "id" : "28870687"
          },
          {
            "name" : "Laravel",
            "screen_name" : "laravelphp",
            "indices" : [
              "24",
              "35"
            ],
            "id_str" : "301440448",
            "id" : "301440448"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/taylorotwell/status/1281927390797725696/photo/1",
            "source_status_id" : "1281927390797725696",
            "indices" : [
              "96",
              "119"
            ],
            "url" : "https://t.co/d47EdNzLU6",
            "media_url" : "http://pbs.twimg.com/media/EcpSKaHWkAI6c8A.png",
            "id_str" : "1281927382702657538",
            "source_user_id" : "28870687",
            "id" : "1281927382702657538",
            "media_url_https" : "https://pbs.twimg.com/media/EcpSKaHWkAI6c8A.png",
            "source_user_id_str" : "28870687",
            "sizes" : {
              "medium" : {
                "w" : "554",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "946",
                "h" : "2048",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "314",
                "h" : "680",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1281927390797725696",
            "display_url" : "pic.twitter.com/d47EdNzLU6"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "119"
      ],
      "favorite_count" : "0",
      "id_str" : "1281966856882974721",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1281966856882974721",
      "possibly_sensitive" : false,
      "created_at" : "Sat Jul 11 15:01:31 +0000 2020",
      "favorited" : false,
      "full_text" : "RT @taylorotwell: Nice! @laravelphp becomes first PHP repository to have 60,000 GitHub stars ⭐️ https://t.co/d47EdNzLU6",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/taylorotwell/status/1281927390797725696/photo/1",
            "source_status_id" : "1281927390797725696",
            "indices" : [
              "96",
              "119"
            ],
            "url" : "https://t.co/d47EdNzLU6",
            "media_url" : "http://pbs.twimg.com/media/EcpSKaHWkAI6c8A.png",
            "id_str" : "1281927382702657538",
            "source_user_id" : "28870687",
            "id" : "1281927382702657538",
            "media_url_https" : "https://pbs.twimg.com/media/EcpSKaHWkAI6c8A.png",
            "source_user_id_str" : "28870687",
            "sizes" : {
              "medium" : {
                "w" : "554",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "946",
                "h" : "2048",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "314",
                "h" : "680",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1281927390797725696",
            "display_url" : "pic.twitter.com/d47EdNzLU6"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1237763930857934848"
          ],
          "editableUntil" : "2020-03-11T16:04:52.315Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [ ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "77"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1237763067024257025",
      "id_str" : "1237763930857934848",
      "in_reply_to_user_id" : "1096775695957602304",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1237763930857934848",
      "in_reply_to_status_id" : "1237763067024257025",
      "created_at" : "Wed Mar 11 15:34:52 +0000 2020",
      "favorited" : false,
      "full_text" : "Catatan: setelah topup dan bertransaksi kembali saldo bonus berhasil dipotong",
      "lang" : "in",
      "in_reply_to_screen_name" : "capsinthehouse",
      "in_reply_to_user_id_str" : "1096775695957602304"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1237763067024257025"
          ],
          "editableUntil" : "2020-03-11T16:01:26.361Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "LinkAja",
            "screen_name" : "linkaja",
            "indices" : [
              "5",
              "13"
            ],
            "id_str" : "3195998996",
            "id" : "3195998996"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1237763067024257025/photo/1",
            "indices" : [
              "170",
              "193"
            ],
            "url" : "https://t.co/NWsWlksspG",
            "media_url" : "http://pbs.twimg.com/media/ES1q8NiU4AEfK1c.jpg",
            "id_str" : "1237763055255085057",
            "id" : "1237763055255085057",
            "media_url_https" : "https://pbs.twimg.com/media/ES1q8NiU4AEfK1c.jpg",
            "sizes" : {
              "small" : {
                "w" : "314",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "828",
                "h" : "1792",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "554",
                "h" : "1200",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/NWsWlksspG"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "193"
      ],
      "favorite_count" : "0",
      "id_str" : "1237763067024257025",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1237763067024257025",
      "possibly_sensitive" : false,
      "created_at" : "Wed Mar 11 15:31:26 +0000 2020",
      "favorited" : false,
      "full_text" : "Halo @linkaja kenapa saat bertransaksi di KlikIndomaret saldo bonus saya dinyatakan nol padahal menurut aplikasi ada saldo bonus Rp 9.900 yang akan berakhir akhir tahun? https://t.co/NWsWlksspG",
      "lang" : "in",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1237763067024257025/photo/1",
            "indices" : [
              "170",
              "193"
            ],
            "url" : "https://t.co/NWsWlksspG",
            "media_url" : "http://pbs.twimg.com/media/ES1q8NiU4AEfK1c.jpg",
            "id_str" : "1237763055255085057",
            "id" : "1237763055255085057",
            "media_url_https" : "https://pbs.twimg.com/media/ES1q8NiU4AEfK1c.jpg",
            "sizes" : {
              "small" : {
                "w" : "314",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "828",
                "h" : "1792",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "554",
                "h" : "1200",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/NWsWlksspG"
          },
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1237763067024257025/photo/1",
            "indices" : [
              "170",
              "193"
            ],
            "url" : "https://t.co/NWsWlksspG",
            "media_url" : "http://pbs.twimg.com/media/ES1q8NhU8AAeGDr.jpg",
            "id_str" : "1237763055250894848",
            "id" : "1237763055250894848",
            "media_url_https" : "https://pbs.twimg.com/media/ES1q8NhU8AAeGDr.jpg",
            "sizes" : {
              "medium" : {
                "w" : "615",
                "h" : "1200",
                "resize" : "fit"
              },
              "small" : {
                "w" : "349",
                "h" : "680",
                "resize" : "fit"
              },
              "large" : {
                "w" : "827",
                "h" : "1613",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/NWsWlksspG"
          },
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1237763067024257025/photo/1",
            "indices" : [
              "170",
              "193"
            ],
            "url" : "https://t.co/NWsWlksspG",
            "media_url" : "http://pbs.twimg.com/media/ES1q8NiUUAEWk7-.jpg",
            "id_str" : "1237763055255048193",
            "id" : "1237763055255048193",
            "media_url_https" : "https://pbs.twimg.com/media/ES1q8NiUUAEWk7-.jpg",
            "sizes" : {
              "medium" : {
                "w" : "554",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "828",
                "h" : "1792",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "314",
                "h" : "680",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/NWsWlksspG"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1223289700590346240"
          ],
          "editableUntil" : "2020-01-31T17:29:26.901Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "John Wilander 🇺🇦",
            "screen_name" : "johnwilander",
            "indices" : [
              "0",
              "13"
            ],
            "id_str" : "57622045",
            "id" : "57622045"
          },
          {
            "name" : "Maciej Stachowiak 🇵🇱🇺🇦",
            "screen_name" : "othermaciej",
            "indices" : [
              "14",
              "26"
            ],
            "id_str" : "14991165",
            "id" : "14991165"
          },
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "27",
              "35"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          },
          {
            "name" : "Firefox Nightly 🔥",
            "screen_name" : "FirefoxNightly",
            "indices" : [
              "36",
              "51"
            ],
            "id_str" : "382769917",
            "id" : "382769917"
          },
          {
            "name" : "WebKit",
            "screen_name" : "webkit",
            "indices" : [
              "52",
              "59"
            ],
            "id_str" : "14315023",
            "id" : "14315023"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "60",
              "73"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "311"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1223284239724007429",
      "id_str" : "1223289700590346240",
      "in_reply_to_user_id" : "57622045",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1223289700590346240",
      "in_reply_to_status_id" : "1223284239724007429",
      "created_at" : "Fri Jan 31 16:59:26 +0000 2020",
      "favorited" : false,
      "full_text" : "@johnwilander @othermaciej @firefox @FirefoxNightly @webkit @reinhart1010 Sure but instead of free WiFis this script injection applies to home internet subscribers (i.e. IndiHome).\n\nThe ISP also owns a mobile carrier (Telkomsel) but at the time of this writing Telkomsel users were unaffected by this injection.",
      "lang" : "en",
      "in_reply_to_screen_name" : "johnwilander",
      "in_reply_to_user_id_str" : "57622045"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1223279772156841984"
          ],
          "editableUntil" : "2020-01-31T16:49:59.778Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Maciej Stachowiak 🇵🇱🇺🇦",
            "screen_name" : "othermaciej",
            "indices" : [
              "0",
              "12"
            ],
            "id_str" : "14991165",
            "id" : "14991165"
          },
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "13",
              "21"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          },
          {
            "name" : "Firefox Nightly 🔥",
            "screen_name" : "FirefoxNightly",
            "indices" : [
              "22",
              "37"
            ],
            "id_str" : "382769917",
            "id" : "382769917"
          },
          {
            "name" : "WebKit",
            "screen_name" : "webkit",
            "indices" : [
              "38",
              "45"
            ],
            "id_str" : "14315023",
            "id" : "14315023"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "46",
              "59"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          },
          {
            "name" : "John Wilander 🇺🇦",
            "screen_name" : "johnwilander",
            "indices" : [
              "60",
              "73"
            ],
            "id_str" : "57622045",
            "id" : "57622045"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "87"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1223279502211477505",
      "id_str" : "1223279772156841984",
      "in_reply_to_user_id" : "14991165",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1223279772156841984",
      "in_reply_to_status_id" : "1223279502211477505",
      "created_at" : "Fri Jan 31 16:19:59 +0000 2020",
      "favorited" : false,
      "full_text" : "@othermaciej @firefox @FirefoxNightly @webkit @reinhart1010 @johnwilander In short, yes",
      "lang" : "en",
      "in_reply_to_screen_name" : "othermaciej",
      "in_reply_to_user_id_str" : "14991165"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1223207188883070981"
          ],
          "editableUntil" : "2020-01-31T12:01:34.577Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Maciej Stachowiak 🇵🇱🇺🇦",
            "screen_name" : "othermaciej",
            "indices" : [
              "0",
              "12"
            ],
            "id_str" : "14991165",
            "id" : "14991165"
          },
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "13",
              "21"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          },
          {
            "name" : "Firefox Nightly 🔥",
            "screen_name" : "FirefoxNightly",
            "indices" : [
              "22",
              "37"
            ],
            "id_str" : "382769917",
            "id" : "382769917"
          },
          {
            "name" : "WebKit",
            "screen_name" : "webkit",
            "indices" : [
              "38",
              "45"
            ],
            "id_str" : "14315023",
            "id" : "14315023"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "46",
              "59"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          },
          {
            "name" : "John Wilander 🇺🇦",
            "screen_name" : "johnwilander",
            "indices" : [
              "60",
              "73"
            ],
            "id_str" : "57622045",
            "id" : "57622045"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/ZuIJid2r1s",
            "expanded_url" : "https://stackoverflow.com/questions/30076093/how-to-clean-up-ads-injection-on-wordpress-which-injected-through-isp",
            "display_url" : "stackoverflow.com/questions/3007…",
            "indices" : [
              "279",
              "302"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "302"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1223206045650022400",
      "id_str" : "1223207188883070981",
      "in_reply_to_user_id" : "1096775695957602304",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1223207188883070981",
      "in_reply_to_status_id" : "1223206045650022400",
      "possibly_sensitive" : false,
      "created_at" : "Fri Jan 31 11:31:34 +0000 2020",
      "favorited" : false,
      "full_text" : "@othermaciej @firefox @FirefoxNightly @webkit @reinhart1010 @johnwilander Despite that the Reddit author posted this a few days ago, this script injection has occured for years.\n\nFor example, this StackOverflow post that suggest the same script is used to inject advertisements:\nhttps://t.co/ZuIJid2r1s",
      "lang" : "en",
      "in_reply_to_screen_name" : "capsinthehouse",
      "in_reply_to_user_id_str" : "1096775695957602304"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1223206045650022400"
          ],
          "editableUntil" : "2020-01-31T11:57:02.009Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Maciej Stachowiak 🇵🇱🇺🇦",
            "screen_name" : "othermaciej",
            "indices" : [
              "0",
              "12"
            ],
            "id_str" : "14991165",
            "id" : "14991165"
          },
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "13",
              "21"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          },
          {
            "name" : "Firefox Nightly 🔥",
            "screen_name" : "FirefoxNightly",
            "indices" : [
              "22",
              "37"
            ],
            "id_str" : "382769917",
            "id" : "382769917"
          },
          {
            "name" : "WebKit",
            "screen_name" : "webkit",
            "indices" : [
              "38",
              "45"
            ],
            "id_str" : "14315023",
            "id" : "14315023"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "46",
              "59"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          },
          {
            "name" : "John Wilander 🇺🇦",
            "screen_name" : "johnwilander",
            "indices" : [
              "60",
              "73"
            ],
            "id_str" : "57622045",
            "id" : "57622045"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "313"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1223204884435988481",
      "id_str" : "1223206045650022400",
      "in_reply_to_user_id" : "1096775695957602304",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1223206045650022400",
      "in_reply_to_status_id" : "1223204884435988481",
      "created_at" : "Fri Jan 31 11:27:02 +0000 2020",
      "favorited" : false,
      "full_text" : "@othermaciej @firefox @FirefoxNightly @webkit @reinhart1010 @johnwilander Based on the code above the script is intended to gather several information such as viewport sizes and current domain, which is likely used for analytics. However, customers feared that this might be used by the ISP for tracking purposes.",
      "lang" : "en",
      "in_reply_to_screen_name" : "capsinthehouse",
      "in_reply_to_user_id_str" : "1096775695957602304"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1223204884435988481"
          ],
          "editableUntil" : "2020-01-31T11:52:25.154Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Maciej Stachowiak 🇵🇱🇺🇦",
            "screen_name" : "othermaciej",
            "indices" : [
              "0",
              "12"
            ],
            "id_str" : "14991165",
            "id" : "14991165"
          },
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "13",
              "21"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          },
          {
            "name" : "Firefox Nightly 🔥",
            "screen_name" : "FirefoxNightly",
            "indices" : [
              "22",
              "37"
            ],
            "id_str" : "382769917",
            "id" : "382769917"
          },
          {
            "name" : "WebKit",
            "screen_name" : "webkit",
            "indices" : [
              "38",
              "45"
            ],
            "id_str" : "14315023",
            "id" : "14315023"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "46",
              "59"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          },
          {
            "name" : "John Wilander 🇺🇦",
            "screen_name" : "johnwilander",
            "indices" : [
              "60",
              "73"
            ],
            "id_str" : "57622045",
            "id" : "57622045"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "319"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1223203122429886466",
      "id_str" : "1223204884435988481",
      "in_reply_to_user_id" : "1096775695957602304",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1223204884435988481",
      "in_reply_to_status_id" : "1223203122429886466",
      "created_at" : "Fri Jan 31 11:22:25 +0000 2020",
      "favorited" : false,
      "full_text" : "@othermaciej @firefox @FirefoxNightly @webkit @reinhart1010 @johnwilander The tracking script never appeared when the (non-HTTPS) site was visited through another ISP, using VPN/DoH, or even when the connection was upgraded to HTTPS.\n\nOne customer also confirmed this script appeared regardless of devices and browsers.",
      "lang" : "en",
      "in_reply_to_screen_name" : "capsinthehouse",
      "in_reply_to_user_id_str" : "1096775695957602304"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1223203122429886466"
          ],
          "editableUntil" : "2020-01-31T11:45:25.059Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Maciej Stachowiak 🇵🇱🇺🇦",
            "screen_name" : "othermaciej",
            "indices" : [
              "0",
              "12"
            ],
            "id_str" : "14991165",
            "id" : "14991165"
          },
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "13",
              "21"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          },
          {
            "name" : "Firefox Nightly 🔥",
            "screen_name" : "FirefoxNightly",
            "indices" : [
              "22",
              "37"
            ],
            "id_str" : "382769917",
            "id" : "382769917"
          },
          {
            "name" : "WebKit",
            "screen_name" : "webkit",
            "indices" : [
              "38",
              "45"
            ],
            "id_str" : "14315023",
            "id" : "14315023"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "46",
              "59"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          },
          {
            "name" : "John Wilander 🇺🇦",
            "screen_name" : "johnwilander",
            "indices" : [
              "60",
              "73"
            ],
            "id_str" : "57622045",
            "id" : "57622045"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1223203122429886466/photo/1",
            "indices" : [
              "262",
              "285"
            ],
            "url" : "https://t.co/BibBcGzPnV",
            "media_url" : "http://pbs.twimg.com/media/EPmwwEGUcAES2N0.jpg",
            "id_str" : "1223203113588256769",
            "id" : "1223203113588256769",
            "media_url_https" : "https://pbs.twimg.com/media/EPmwwEGUcAES2N0.jpg",
            "sizes" : {
              "large" : {
                "w" : "669",
                "h" : "333",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "669",
                "h" : "333",
                "resize" : "fit"
              },
              "small" : {
                "w" : "669",
                "h" : "333",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/BibBcGzPnV"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "285"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1223202469989117953",
      "id_str" : "1223203122429886466",
      "in_reply_to_user_id" : "1096775695957602304",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1223203122429886466",
      "in_reply_to_status_id" : "1223202469989117953",
      "possibly_sensitive" : false,
      "created_at" : "Fri Jan 31 11:15:25 +0000 2020",
      "favorited" : false,
      "full_text" : "@othermaciej @firefox @FirefoxNightly @webkit @reinhart1010 @johnwilander Some Telkom Indonesia (an ISP and state-owned enterprise) customers noticed the addition of a tracking script embedded at the end of non-HTTPS websites and web pages that looks like this: https://t.co/BibBcGzPnV",
      "lang" : "en",
      "in_reply_to_screen_name" : "capsinthehouse",
      "in_reply_to_user_id_str" : "1096775695957602304",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1223203122429886466/photo/1",
            "indices" : [
              "262",
              "285"
            ],
            "url" : "https://t.co/BibBcGzPnV",
            "media_url" : "http://pbs.twimg.com/media/EPmwwEGUcAES2N0.jpg",
            "id_str" : "1223203113588256769",
            "id" : "1223203113588256769",
            "media_url_https" : "https://pbs.twimg.com/media/EPmwwEGUcAES2N0.jpg",
            "sizes" : {
              "large" : {
                "w" : "669",
                "h" : "333",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "669",
                "h" : "333",
                "resize" : "fit"
              },
              "small" : {
                "w" : "669",
                "h" : "333",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/BibBcGzPnV"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1223202469989117953"
          ],
          "editableUntil" : "2020-01-31T11:42:49.505Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Maciej Stachowiak 🇵🇱🇺🇦",
            "screen_name" : "othermaciej",
            "indices" : [
              "0",
              "12"
            ],
            "id_str" : "14991165",
            "id" : "14991165"
          },
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "13",
              "21"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          },
          {
            "name" : "Firefox Nightly 🔥",
            "screen_name" : "FirefoxNightly",
            "indices" : [
              "22",
              "37"
            ],
            "id_str" : "382769917",
            "id" : "382769917"
          },
          {
            "name" : "WebKit",
            "screen_name" : "webkit",
            "indices" : [
              "38",
              "45"
            ],
            "id_str" : "14315023",
            "id" : "14315023"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "46",
              "59"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          },
          {
            "name" : "John Wilander 🇺🇦",
            "screen_name" : "johnwilander",
            "indices" : [
              "60",
              "73"
            ],
            "id_str" : "57622045",
            "id" : "57622045"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/9S0U5gxpyw",
            "expanded_url" : "https://www.reddit.com/r/indonesia/comments/evkqns/beaware_tracking_script_isp_telkom/",
            "display_url" : "reddit.com/r/indonesia/co…",
            "indices" : [
              "295",
              "318"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "318"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1222981249737752576",
      "id_str" : "1223202469989117953",
      "in_reply_to_user_id" : "14991165",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1223202469989117953",
      "in_reply_to_status_id" : "1222981249737752576",
      "possibly_sensitive" : false,
      "created_at" : "Fri Jan 31 11:12:49 +0000 2020",
      "favorited" : false,
      "full_text" : "@othermaciej @firefox @FirefoxNightly @webkit @reinhart1010 @johnwilander Here is the original post on Reddit. Reddit is blocked by the Indonesian government (for explicit content reasons, same with Vimeo) but some users are still able to access the service via VPN and DNS-over-HTTPS services\n https://t.co/9S0U5gxpyw",
      "lang" : "en",
      "in_reply_to_screen_name" : "othermaciej",
      "in_reply_to_user_id_str" : "14991165"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1222725966176194560"
          ],
          "editableUntil" : "2020-01-30T04:09:22.143Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "EFF",
            "screen_name" : "EFF",
            "indices" : [
              "5",
              "9"
            ],
            "id_str" : "4816",
            "id" : "4816"
          },
          {
            "name" : "DuckDuckGo",
            "screen_name" : "DuckDuckGo",
            "indices" : [
              "14",
              "25"
            ],
            "id_str" : "14504859",
            "id" : "14504859"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "25"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1222723813747486721",
      "id_str" : "1222725966176194560",
      "in_reply_to_user_id" : "1096775695957602304",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1222725966176194560",
      "in_reply_to_status_id" : "1222723813747486721",
      "created_at" : "Thu Jan 30 03:39:22 +0000 2020",
      "favorited" : false,
      "full_text" : "Also @EFF and @DuckDuckGo",
      "lang" : "en",
      "in_reply_to_screen_name" : "capsinthehouse",
      "in_reply_to_user_id_str" : "1096775695957602304"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1222723813747486721"
          ],
          "editableUntil" : "2020-01-30T04:00:48.964Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "0",
              "8"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          },
          {
            "name" : "Firefox Nightly 🔥",
            "screen_name" : "FirefoxNightly",
            "indices" : [
              "9",
              "24"
            ],
            "id_str" : "382769917",
            "id" : "382769917"
          },
          {
            "name" : "WebKit",
            "screen_name" : "webkit",
            "indices" : [
              "25",
              "32"
            ],
            "id_str" : "14315023",
            "id" : "14315023"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "130",
              "143"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/lVkoWBdv8v",
            "expanded_url" : "https://twitter.com/rednesia/status/1222506907065176070",
            "display_url" : "twitter.com/rednesia/statu…",
            "indices" : [
              "145",
              "168"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "168"
      ],
      "favorite_count" : "0",
      "id_str" : "1222723813747486721",
      "in_reply_to_user_id" : "2142731",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1222723813747486721",
      "possibly_sensitive" : false,
      "created_at" : "Thu Jan 30 03:30:48 +0000 2020",
      "favorited" : false,
      "full_text" : "@firefox @FirefoxNightly @webkit where can I get in touch with Intelligent Tracking Protection dev team?\n\n(Available on GitHub as @reinhart1010) https://t.co/lVkoWBdv8v",
      "lang" : "en",
      "in_reply_to_screen_name" : "firefox",
      "in_reply_to_user_id_str" : "2142731"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1216623854807764993"
          ],
          "editableUntil" : "2020-01-13T08:01:45.421Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"https://mobile.twitter.com\" rel=\"nofollow\">Twitter Web App</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Roskilde Festival",
            "screen_name" : "orangefeeling",
            "indices" : [
              "6",
              "20"
            ],
            "id_str" : "27261173",
            "id" : "27261173"
          },
          {
            "name" : "webcompat.com",
            "screen_name" : "webcompat",
            "indices" : [
              "203",
              "213"
            ],
            "id_str" : "2244587504",
            "id" : "2244587504"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/F1Mt1pEnrL",
            "expanded_url" : "https://github.com/webcompat/web-bugs/issues/45077",
            "display_url" : "github.com/webcompat/web-…",
            "indices" : [
              "175",
              "198"
            ]
          }
        ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1216623854807764993/photo/1",
            "indices" : [
              "214",
              "237"
            ],
            "url" : "https://t.co/0OLKeI0IxW",
            "media_url" : "http://pbs.twimg.com/media/EOJQ65nUYAIbTny.jpg",
            "id_str" : "1216623822171824130",
            "id" : "1216623822171824130",
            "media_url_https" : "https://pbs.twimg.com/media/EOJQ65nUYAIbTny.jpg",
            "sizes" : {
              "medium" : {
                "w" : "1200",
                "h" : "238",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "2048",
                "h" : "405",
                "resize" : "fit"
              },
              "small" : {
                "w" : "680",
                "h" : "135",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/0OLKeI0IxW"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "237"
      ],
      "favorite_count" : "0",
      "id_str" : "1216623854807764993",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1216623854807764993",
      "possibly_sensitive" : false,
      "created_at" : "Mon Jan 13 07:31:45 +0000 2020",
      "favorited" : false,
      "full_text" : "Hello @orangefeeling I'd like to report an issue with your site in Firefox. (Left: Firefox 72.0a1, Right: Chrome 78.0.3904.108)\n\nMore details of this issue can be found here:\nhttps://t.co/F1Mt1pEnrL\n\ncc @webcompat https://t.co/0OLKeI0IxW",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1216623854807764993/photo/1",
            "indices" : [
              "214",
              "237"
            ],
            "url" : "https://t.co/0OLKeI0IxW",
            "media_url" : "http://pbs.twimg.com/media/EOJQ65nUYAIbTny.jpg",
            "id_str" : "1216623822171824130",
            "id" : "1216623822171824130",
            "media_url_https" : "https://pbs.twimg.com/media/EOJQ65nUYAIbTny.jpg",
            "sizes" : {
              "medium" : {
                "w" : "1200",
                "h" : "238",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "2048",
                "h" : "405",
                "resize" : "fit"
              },
              "small" : {
                "w" : "680",
                "h" : "135",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/0OLKeI0IxW"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1210181820915957760"
          ],
          "editableUntil" : "2019-12-26T13:23:24.854Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Cassidy",
            "screen_name" : "cassidoo",
            "indices" : [
              "3",
              "12"
            ],
            "id_str" : "400286802",
            "id" : "400286802"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/cassidoo/status/1209542529055678464/video/1",
            "source_status_id" : "1209542529055678464",
            "indices" : [
              "56",
              "79"
            ],
            "url" : "https://t.co/X5yWiVDE84",
            "media_url" : "http://pbs.twimg.com/ext_tw_video_thumb/1209542471413354497/pu/img/SMwC_GWJTwI84rOk.jpg",
            "id_str" : "1209542471413354497",
            "source_user_id" : "400286802",
            "id" : "1209542471413354497",
            "media_url_https" : "https://pbs.twimg.com/ext_tw_video_thumb/1209542471413354497/pu/img/SMwC_GWJTwI84rOk.jpg",
            "source_user_id_str" : "400286802",
            "sizes" : {
              "large" : {
                "w" : "720",
                "h" : "1058",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "720",
                "h" : "1058",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "463",
                "h" : "680",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1209542529055678464",
            "display_url" : "pic.twitter.com/X5yWiVDE84"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "79"
      ],
      "favorite_count" : "0",
      "id_str" : "1210181820915957760",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1210181820915957760",
      "possibly_sensitive" : false,
      "created_at" : "Thu Dec 26 12:53:24 +0000 2019",
      "favorited" : false,
      "full_text" : "RT @cassidoo: let yourself have a var-y merry const-mas https://t.co/X5yWiVDE84",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/cassidoo/status/1209542529055678464/video/1",
            "source_status_id" : "1209542529055678464",
            "indices" : [
              "56",
              "79"
            ],
            "url" : "https://t.co/X5yWiVDE84",
            "media_url" : "http://pbs.twimg.com/ext_tw_video_thumb/1209542471413354497/pu/img/SMwC_GWJTwI84rOk.jpg",
            "id_str" : "1209542471413354497",
            "video_info" : {
              "aspect_ratio" : [
                "360",
                "529"
              ],
              "duration_millis" : "10233",
              "variants" : [
                {
                  "content_type" : "application/x-mpegURL",
                  "url" : "https://video.twimg.com/ext_tw_video/1209542471413354497/pu/pl/Dcz2uKyqOAaUf9_N.m3u8?tag=10"
                },
                {
                  "bitrate" : "832000",
                  "content_type" : "video/mp4",
                  "url" : "https://video.twimg.com/ext_tw_video/1209542471413354497/pu/vid/360x528/NW7HPO8o-bVbc_Q_.mp4?tag=10"
                },
                {
                  "bitrate" : "2176000",
                  "content_type" : "video/mp4",
                  "url" : "https://video.twimg.com/ext_tw_video/1209542471413354497/pu/vid/720x1058/d_SkK_1ymhaWCuvr.mp4?tag=10"
                },
                {
                  "bitrate" : "632000",
                  "content_type" : "video/mp4",
                  "url" : "https://video.twimg.com/ext_tw_video/1209542471413354497/pu/vid/320x470/cJl1J3wA6Cev2JdF.mp4?tag=10"
                }
              ]
            },
            "source_user_id" : "400286802",
            "additional_media_info" : {
              "monetizable" : false
            },
            "id" : "1209542471413354497",
            "media_url_https" : "https://pbs.twimg.com/ext_tw_video_thumb/1209542471413354497/pu/img/SMwC_GWJTwI84rOk.jpg",
            "source_user_id_str" : "400286802",
            "sizes" : {
              "large" : {
                "w" : "720",
                "h" : "1058",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "720",
                "h" : "1058",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "463",
                "h" : "680",
                "resize" : "fit"
              }
            },
            "type" : "video",
            "source_status_id_str" : "1209542529055678464",
            "display_url" : "pic.twitter.com/X5yWiVDE84"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1203666492148969472"
          ],
          "editableUntil" : "2019-12-08T13:53:49.427Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"https://mobile.twitter.com\" rel=\"nofollow\">Twitter Web App</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "kitze 🚀",
            "screen_name" : "thekitze",
            "indices" : [
              "3",
              "12"
            ],
            "id_str" : "50339173",
            "id" : "50339173"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "94"
      ],
      "favorite_count" : "0",
      "id_str" : "1203666492148969472",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1203666492148969472",
      "created_at" : "Sun Dec 08 13:23:49 +0000 2019",
      "favorited" : false,
      "full_text" : "RT @thekitze: he's making a list\nand checking it twice\nbecause he’s still not using typescript",
      "lang" : "en"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1195186291186401280"
          ],
          "editableUntil" : "2019-11-15T04:16:31.963Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"https://mobile.twitter.com\" rel=\"nofollow\">Twitter Web App</a>",
      "entities" : {
        "hashtags" : [
          {
            "text" : "GrabExpress",
            "indices" : [
              "67",
              "79"
            ]
          },
          {
            "text" : "Webcompat",
            "indices" : [
              "80",
              "90"
            ]
          }
        ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Grab Singapore",
            "screen_name" : "GrabSG",
            "indices" : [
              "0",
              "7"
            ],
            "id_str" : "1724451330",
            "id" : "1724451330"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/lwejvcBAOG",
            "expanded_url" : "https://twitter.com/capsinthehouse/status/1170870226591682560",
            "display_url" : "twitter.com/capsinthehouse…",
            "indices" : [
              "91",
              "114"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "114"
      ],
      "favorite_count" : "0",
      "id_str" : "1195186291186401280",
      "in_reply_to_user_id" : "1724451330",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1195186291186401280",
      "possibly_sensitive" : false,
      "created_at" : "Fri Nov 15 03:46:31 +0000 2019",
      "favorited" : false,
      "full_text" : "@GrabSG How is this going? The issue still persists on Firefox 70. #GrabExpress #Webcompat https://t.co/lwejvcBAOG",
      "lang" : "en",
      "in_reply_to_screen_name" : "GrabSG",
      "in_reply_to_user_id_str" : "1724451330"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1190789107506540544"
          ],
          "editableUntil" : "2019-11-03T01:03:41.678Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "T❘LM△N",
            "screen_name" : "Tilman",
            "indices" : [
              "3",
              "10"
            ],
            "id_str" : "60383",
            "id" : "60383"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/Tilman/status/1190729271318331394/photo/1",
            "source_status_id" : "1190729271318331394",
            "indices" : [
              "111",
              "134"
            ],
            "url" : "https://t.co/gEkmw1g0ol",
            "media_url" : "http://pbs.twimg.com/media/EIZR8ytW4AEM95F.jpg",
            "id_str" : "1190729256332091393",
            "source_user_id" : "60383",
            "id" : "1190729256332091393",
            "media_url_https" : "https://pbs.twimg.com/media/EIZR8ytW4AEM95F.jpg",
            "source_user_id_str" : "60383",
            "sizes" : {
              "medium" : {
                "w" : "1200",
                "h" : "407",
                "resize" : "fit"
              },
              "large" : {
                "w" : "2010",
                "h" : "682",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "680",
                "h" : "231",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1190729271318331394",
            "display_url" : "pic.twitter.com/gEkmw1g0ol"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "134"
      ],
      "favorite_count" : "0",
      "id_str" : "1190789107506540544",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1190789107506540544",
      "possibly_sensitive" : false,
      "created_at" : "Sun Nov 03 00:33:41 +0000 2019",
      "favorited" : false,
      "full_text" : "RT @Tilman: New Microsoft Edge Logo looks a lot like Firefox, inverted and rotated 180°. Well done, designers. https://t.co/gEkmw1g0ol",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/Tilman/status/1190729271318331394/photo/1",
            "source_status_id" : "1190729271318331394",
            "indices" : [
              "111",
              "134"
            ],
            "url" : "https://t.co/gEkmw1g0ol",
            "media_url" : "http://pbs.twimg.com/media/EIZR8ytW4AEM95F.jpg",
            "id_str" : "1190729256332091393",
            "source_user_id" : "60383",
            "id" : "1190729256332091393",
            "media_url_https" : "https://pbs.twimg.com/media/EIZR8ytW4AEM95F.jpg",
            "source_user_id_str" : "60383",
            "sizes" : {
              "medium" : {
                "w" : "1200",
                "h" : "407",
                "resize" : "fit"
              },
              "large" : {
                "w" : "2010",
                "h" : "682",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "680",
                "h" : "231",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1190729271318331394",
            "display_url" : "pic.twitter.com/gEkmw1g0ol"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1188453248417456129"
          ],
          "editableUntil" : "2019-10-27T14:21:49.473Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"https://mobile.twitter.com\" rel=\"nofollow\">Twitter Web App</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Code.org",
            "screen_name" : "codeorg",
            "indices" : [
              "0",
              "8"
            ],
            "id_str" : "850107536",
            "id" : "850107536"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/KTeqiKBRnr",
            "expanded_url" : "https://hourofcode.com/id/",
            "display_url" : "hourofcode.com/id/",
            "indices" : [
              "71",
              "94"
            ]
          }
        ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1188453248417456129/photo/1",
            "indices" : [
              "95",
              "118"
            ],
            "url" : "https://t.co/dqPMp0tWFE",
            "media_url" : "http://pbs.twimg.com/media/EH477V-U0AES-qY.png",
            "id_str" : "1188453242369265665",
            "id" : "1188453242369265665",
            "media_url_https" : "https://pbs.twimg.com/media/EH477V-U0AES-qY.png",
            "sizes" : {
              "medium" : {
                "w" : "1140",
                "h" : "276",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "680",
                "h" : "165",
                "resize" : "fit"
              },
              "large" : {
                "w" : "1140",
                "h" : "276",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/dqPMp0tWFE"
          }
        ],
        "hashtags" : [
          {
            "text" : "HourofCode",
            "indices" : [
              "50",
              "61"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "118"
      ],
      "favorite_count" : "0",
      "id_str" : "1188453248417456129",
      "in_reply_to_user_id" : "850107536",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1188453248417456129",
      "possibly_sensitive" : false,
      "created_at" : "Sun Oct 27 13:51:49 +0000 2019",
      "favorited" : false,
      "full_text" : "@codeorg Seems like a %{localization_bug} on this #HourofCode webpage: https://t.co/KTeqiKBRnr https://t.co/dqPMp0tWFE",
      "lang" : "en",
      "in_reply_to_screen_name" : "codeorg",
      "in_reply_to_user_id_str" : "850107536",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1188453248417456129/photo/1",
            "indices" : [
              "95",
              "118"
            ],
            "url" : "https://t.co/dqPMp0tWFE",
            "media_url" : "http://pbs.twimg.com/media/EH477V-U0AES-qY.png",
            "id_str" : "1188453242369265665",
            "id" : "1188453242369265665",
            "media_url_https" : "https://pbs.twimg.com/media/EH477V-U0AES-qY.png",
            "sizes" : {
              "medium" : {
                "w" : "1140",
                "h" : "276",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "680",
                "h" : "165",
                "resize" : "fit"
              },
              "large" : {
                "w" : "1140",
                "h" : "276",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/dqPMp0tWFE"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1184500755723505664"
          ],
          "editableUntil" : "2019-10-16T16:36:01.781Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Evie",
            "screen_name" : "useevie",
            "indices" : [
              "0",
              "8"
            ],
            "id_str" : "1680135374",
            "id" : "1680135374"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "227"
      ],
      "favorite_count" : "0",
      "id_str" : "1184500755723505664",
      "in_reply_to_user_id" : "1680135374",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1184500755723505664",
      "created_at" : "Wed Oct 16 16:06:01 +0000 2019",
      "favorited" : false,
      "full_text" : "@useevie How to clear notification count badge cache in Evie Launcher? There's at least one app that gives 99+ count in the Launcher which cannot be cleared after opening the notification and even after Android software update.",
      "lang" : "en",
      "in_reply_to_screen_name" : "useevie",
      "in_reply_to_user_id_str" : "1680135374"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1182474156064960512"
          ],
          "editableUntil" : "2019-10-11T02:23:02.770Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Telegram Messenger",
            "screen_name" : "telegram",
            "indices" : [
              "0",
              "9"
            ],
            "id_str" : "1689053928",
            "id" : "1689053928"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/4YghJNbAhC",
            "expanded_url" : "https://github.com/webcompat/web-bugs/issues/35431",
            "display_url" : "github.com/webcompat/web-…",
            "indices" : [
              "141",
              "164"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "164"
      ],
      "favorite_count" : "1",
      "id_str" : "1182474156064960512",
      "in_reply_to_user_id" : "1689053928",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1182474156064960512",
      "possibly_sensitive" : false,
      "created_at" : "Fri Oct 11 01:53:02 +0000 2019",
      "favorited" : false,
      "full_text" : "@telegram Telegraph is somehow broken in Firefox for Android, where captions for photos and videos are impossible to be entered.\n\nMore info: https://t.co/4YghJNbAhC",
      "lang" : "en",
      "in_reply_to_screen_name" : "telegram",
      "in_reply_to_user_id_str" : "1689053928"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1181850293413138432"
          ],
          "editableUntil" : "2019-10-09T09:04:02.323Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "DANA.id",
            "screen_name" : "danawallet",
            "indices" : [
              "0",
              "11"
            ],
            "id_str" : "965871936545816577",
            "id" : "965871936545816577"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1181850293413138432/photo/1",
            "indices" : [
              "65",
              "88"
            ],
            "url" : "https://t.co/KUO1gSeeGF",
            "media_url" : "http://pbs.twimg.com/media/EGbGkYvUwAAaCIx.jpg",
            "id_str" : "1181850280649867264",
            "id" : "1181850280649867264",
            "media_url_https" : "https://pbs.twimg.com/media/EGbGkYvUwAAaCIx.jpg",
            "sizes" : {
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/KUO1gSeeGF"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "88"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1181801686546964480",
      "id_str" : "1181850293413138432",
      "in_reply_to_user_id" : "965871936545816577",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1181850293413138432",
      "in_reply_to_status_id" : "1181801686546964480",
      "possibly_sensitive" : false,
      "created_at" : "Wed Oct 09 08:34:02 +0000 2019",
      "favorited" : false,
      "full_text" : "@danawallet Saldo sudah diteruskan beberapa jam setelah masalah: https://t.co/KUO1gSeeGF",
      "lang" : "in",
      "in_reply_to_screen_name" : "danawallet",
      "in_reply_to_user_id_str" : "965871936545816577",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1181850293413138432/photo/1",
            "indices" : [
              "65",
              "88"
            ],
            "url" : "https://t.co/KUO1gSeeGF",
            "media_url" : "http://pbs.twimg.com/media/EGbGkYvUwAAaCIx.jpg",
            "id_str" : "1181850280649867264",
            "id" : "1181850280649867264",
            "media_url_https" : "https://pbs.twimg.com/media/EGbGkYvUwAAaCIx.jpg",
            "sizes" : {
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/KUO1gSeeGF"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1180997487550590976"
          ],
          "editableUntil" : "2019-10-07T00:35:17.561Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "DANA.id",
            "screen_name" : "danawallet",
            "indices" : [
              "50",
              "61"
            ],
            "id_str" : "965871936545816577",
            "id" : "965871936545816577"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1180997487550590976/photo/1",
            "indices" : [
              "190",
              "213"
            ],
            "url" : "https://t.co/Gwkh4xk0lZ",
            "media_url" : "http://pbs.twimg.com/media/EGO-7iUUcAAzdqv.jpg",
            "id_str" : "1180997457334792192",
            "id" : "1180997457334792192",
            "media_url_https" : "https://pbs.twimg.com/media/EGO-7iUUcAAzdqv.jpg",
            "sizes" : {
              "large" : {
                "w" : "1151",
                "h" : "2048",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "382",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "674",
                "h" : "1200",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/Gwkh4xk0lZ"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "213"
      ],
      "favorite_count" : "0",
      "id_str" : "1180997487550590976",
      "in_reply_to_user_id" : "897020835478724608",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1180997487550590976",
      "possibly_sensitive" : false,
      "created_at" : "Mon Oct 07 00:05:17 +0000 2019",
      "favorited" : false,
      "full_text" : "@BLUEmart_ID Saya barusan mengalami masalah topup @danawallet Rp 100.000 di BLUEmart Pasar Tomang Barat (Kopro), setelah memasukkan uang kertas. Apa nanti saldo akan diteruskan ke rekening? https://t.co/Gwkh4xk0lZ",
      "lang" : "in",
      "in_reply_to_user_id_str" : "897020835478724608",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1180997487550590976/photo/1",
            "indices" : [
              "190",
              "213"
            ],
            "url" : "https://t.co/Gwkh4xk0lZ",
            "media_url" : "http://pbs.twimg.com/media/EGO-7iUUcAAzdqv.jpg",
            "id_str" : "1180997457334792192",
            "id" : "1180997457334792192",
            "media_url_https" : "https://pbs.twimg.com/media/EGO-7iUUcAAzdqv.jpg",
            "sizes" : {
              "large" : {
                "w" : "1151",
                "h" : "2048",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "382",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "674",
                "h" : "1200",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/Gwkh4xk0lZ"
          },
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1180997487550590976/photo/1",
            "indices" : [
              "190",
              "213"
            ],
            "url" : "https://t.co/Gwkh4xk0lZ",
            "media_url" : "http://pbs.twimg.com/media/EGO-8UKUEAALTai.jpg",
            "id_str" : "1180997470714597376",
            "id" : "1180997470714597376",
            "media_url_https" : "https://pbs.twimg.com/media/EGO-8UKUEAALTai.jpg",
            "sizes" : {
              "medium" : {
                "w" : "1200",
                "h" : "674",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "680",
                "h" : "382",
                "resize" : "fit"
              },
              "large" : {
                "w" : "2048",
                "h" : "1151",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/Gwkh4xk0lZ"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1173917441816055808"
          ],
          "editableUntil" : "2019-09-17T11:41:43.117Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Gojek Indonesia",
            "screen_name" : "gojekindonesia",
            "indices" : [
              "0",
              "15"
            ],
            "id_str" : "226481275",
            "id" : "226481275"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "118"
      ],
      "favorite_count" : "0",
      "id_str" : "1173917441816055808",
      "in_reply_to_user_id" : "226481275",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1173917441816055808",
      "created_at" : "Tue Sep 17 11:11:43 +0000 2019",
      "favorited" : false,
      "full_text" : "@gojekindonesia Aplikasi GoBiz tidak bisa login dan call center 021-80643109 tidak bisa dihubungi (Telkomsel, Indosat)",
      "lang" : "in",
      "in_reply_to_screen_name" : "gojekindonesia",
      "in_reply_to_user_id_str" : "226481275"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1171223911414910977"
          ],
          "editableUntil" : "2019-09-10T01:18:35.426Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Firefox DevTools",
            "screen_name" : "FirefoxDevTools",
            "indices" : [
              "3",
              "19"
            ],
            "id_str" : "1599870367",
            "id" : "1599870367"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "140"
      ],
      "favorite_count" : "0",
      "id_str" : "1171223911414910977",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1171223911414910977",
      "created_at" : "Tue Sep 10 00:48:35 +0000 2019",
      "favorited" : false,
      "full_text" : "RT @FirefoxDevTools: 🔥 Cross-Browser Compatibility Unite 🔥\n\nThanks to MDN's compat data, caniuse' insights are fresher than ever and easier…",
      "lang" : "en"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1171223830758486016"
          ],
          "editableUntil" : "2019-09-10T01:18:16.196Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "webcompat.com",
            "screen_name" : "webcompat",
            "indices" : [
              "3",
              "13"
            ],
            "id_str" : "2244587504",
            "id" : "2244587504"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "140"
      ],
      "favorite_count" : "0",
      "id_str" : "1171223830758486016",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1171223830758486016",
      "created_at" : "Tue Sep 10 00:48:16 +0000 2019",
      "favorited" : false,
      "full_text" : "RT @webcompat: And it started… Android 9 to Android 10 creates user agent sniffing breakage. (1 digit to 2 digits in detection algorithms.)…",
      "lang" : "en"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1170870226591682560"
          ],
          "editableUntil" : "2019-09-09T01:53:10.393Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Grab Singapore",
            "screen_name" : "GrabSG",
            "indices" : [
              "0",
              "7"
            ],
            "id_str" : "1724451330",
            "id" : "1724451330"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/n4LFJS1xNT",
            "expanded_url" : "https://github.com/webcompat/web-bugs/issues/38293",
            "display_url" : "github.com/webcompat/web-…",
            "indices" : [
              "83",
              "106"
            ]
          },
          {
            "url" : "https://t.co/0GsUv4d4PY",
            "expanded_url" : "https://bugzilla.mozilla.org/show_bug.cgi?id=567039",
            "display_url" : "bugzilla.mozilla.org/show_bug.cgi?i…",
            "indices" : [
              "224",
              "247"
            ]
          }
        ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1170870226591682560/photo/1",
            "indices" : [
              "248",
              "271"
            ],
            "url" : "https://t.co/UbbuGXB28M",
            "media_url" : "http://pbs.twimg.com/media/ED_EQqLU0AE6WUD.jpg",
            "id_str" : "1170870218618359809",
            "id" : "1170870218618359809",
            "media_url_https" : "https://pbs.twimg.com/media/ED_EQqLU0AE6WUD.jpg",
            "sizes" : {
              "large" : {
                "w" : "1366",
                "h" : "744",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "680",
                "h" : "370",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "1200",
                "h" : "654",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/UbbuGXB28M"
          }
        ],
        "hashtags" : [
          {
            "text" : "GrabExpress",
            "indices" : [
              "19",
              "31"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "271"
      ],
      "favorite_count" : "1",
      "id_str" : "1170870226591682560",
      "in_reply_to_user_id" : "1724451330",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1170870226591682560",
      "possibly_sensitive" : false,
      "created_at" : "Mon Sep 09 01:23:10 +0000 2019",
      "favorited" : false,
      "full_text" : "@GrabSG seems that #GrabExpress tracking page is having a problem in Firefox.\n\nSee https://t.co/n4LFJS1xNT for details\n\nHint: \"height:-moz-available\" is unsupported (but \"width:-moz-available\" does) in Firefox, according to https://t.co/0GsUv4d4PY https://t.co/UbbuGXB28M",
      "lang" : "en",
      "in_reply_to_screen_name" : "GrabSG",
      "in_reply_to_user_id_str" : "1724451330",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1170870226591682560/photo/1",
            "indices" : [
              "248",
              "271"
            ],
            "url" : "https://t.co/UbbuGXB28M",
            "media_url" : "http://pbs.twimg.com/media/ED_EQqLU0AE6WUD.jpg",
            "id_str" : "1170870218618359809",
            "id" : "1170870218618359809",
            "media_url_https" : "https://pbs.twimg.com/media/ED_EQqLU0AE6WUD.jpg",
            "sizes" : {
              "large" : {
                "w" : "1366",
                "h" : "744",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "680",
                "h" : "370",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "1200",
                "h" : "654",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/UbbuGXB28M"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1164804611665850368"
          ],
          "editableUntil" : "2019-08-23T08:10:35.101Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "MMCrypto",
            "screen_name" : "AmarachiAmaechi",
            "indices" : [
              "3",
              "19"
            ],
            "id_str" : "1487967471193710597",
            "id" : "1487967471193710597"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/AmarachiAmaechi/status/1164493933352837120/photo/1",
            "source_status_id" : "1164493933352837120",
            "indices" : [
              "73",
              "96"
            ],
            "url" : "https://t.co/OwZRs4uu5Q",
            "media_url" : "http://pbs.twimg.com/media/ECkdDZiX4AE8Bex.jpg",
            "id_str" : "1164493922883854337",
            "source_user_id" : "906300286565863424",
            "id" : "1164493922883854337",
            "media_url_https" : "https://pbs.twimg.com/media/ECkdDZiX4AE8Bex.jpg",
            "source_user_id_str" : "906300286565863424",
            "sizes" : {
              "large" : {
                "w" : "1080",
                "h" : "898",
                "resize" : "fit"
              },
              "small" : {
                "w" : "680",
                "h" : "565",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "1080",
                "h" : "898",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1164493933352837120",
            "display_url" : "pic.twitter.com/OwZRs4uu5Q"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "96"
      ],
      "favorite_count" : "0",
      "id_str" : "1164804611665850368",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1164804611665850368",
      "possibly_sensitive" : false,
      "created_at" : "Fri Aug 23 07:40:35 +0000 2019",
      "favorited" : false,
      "full_text" : "RT @AmarachiAmaechi: Really 🤦🤦 JavaScript also known as java for short🙄🙄 https://t.co/OwZRs4uu5Q",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/AmarachiAmaechi/status/1164493933352837120/photo/1",
            "source_status_id" : "1164493933352837120",
            "indices" : [
              "73",
              "96"
            ],
            "url" : "https://t.co/OwZRs4uu5Q",
            "media_url" : "http://pbs.twimg.com/media/ECkdDZiX4AE8Bex.jpg",
            "id_str" : "1164493922883854337",
            "source_user_id" : "906300286565863424",
            "id" : "1164493922883854337",
            "media_url_https" : "https://pbs.twimg.com/media/ECkdDZiX4AE8Bex.jpg",
            "source_user_id_str" : "906300286565863424",
            "sizes" : {
              "large" : {
                "w" : "1080",
                "h" : "898",
                "resize" : "fit"
              },
              "small" : {
                "w" : "680",
                "h" : "565",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "1080",
                "h" : "898",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1164493933352837120",
            "display_url" : "pic.twitter.com/OwZRs4uu5Q"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1163748569460854784"
          ],
          "editableUntil" : "2019-08-20T10:14:15.019Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "aduankonten",
            "screen_name" : "aduankonten",
            "indices" : [
              "0",
              "12"
            ],
            "id_str" : "995850815184105472",
            "id" : "995850815184105472"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1163748569460854784/photo/1",
            "indices" : [
              "66",
              "89"
            ],
            "url" : "https://t.co/muagMQlPyr",
            "media_url" : "http://pbs.twimg.com/media/ECZ3JYRU8AIs9YY.jpg",
            "id_str" : "1163748556739506178",
            "id" : "1163748556739506178",
            "media_url_https" : "https://pbs.twimg.com/media/ECZ3JYRU8AIs9YY.jpg",
            "sizes" : {
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/muagMQlPyr"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "89"
      ],
      "favorite_count" : "0",
      "id_str" : "1163748569460854784",
      "in_reply_to_user_id" : "995850815184105472",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1163748569460854784",
      "possibly_sensitive" : false,
      "created_at" : "Tue Aug 20 09:44:15 +0000 2019",
      "favorited" : false,
      "full_text" : "@aduankonten Laman pemblokiran Internet Sehat yang kurang \"Sehat\" https://t.co/muagMQlPyr",
      "lang" : "in",
      "in_reply_to_screen_name" : "aduankonten",
      "in_reply_to_user_id_str" : "995850815184105472",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1163748569460854784/photo/1",
            "indices" : [
              "66",
              "89"
            ],
            "url" : "https://t.co/muagMQlPyr",
            "media_url" : "http://pbs.twimg.com/media/ECZ3JYRU8AIs9YY.jpg",
            "id_str" : "1163748556739506178",
            "id" : "1163748556739506178",
            "media_url_https" : "https://pbs.twimg.com/media/ECZ3JYRU8AIs9YY.jpg",
            "sizes" : {
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/muagMQlPyr"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1152874363433000960"
          ],
          "editableUntil" : "2019-07-21T10:04:02.269Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [ ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1152874363433000960/photo/1",
            "indices" : [
              "162",
              "185"
            ],
            "url" : "https://t.co/Cc9TXXKSpr",
            "media_url" : "http://pbs.twimg.com/media/D__VHLSU4AAuMu7.jpg",
            "id_str" : "1152874348895592448",
            "id" : "1152874348895592448",
            "media_url_https" : "https://pbs.twimg.com/media/D__VHLSU4AAuMu7.jpg",
            "sizes" : {
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/Cc9TXXKSpr"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "185"
      ],
      "favorite_count" : "0",
      "id_str" : "1152874363433000960",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1152874363433000960",
      "possibly_sensitive" : false,
      "created_at" : "Sun Jul 21 09:34:02 +0000 2019",
      "favorited" : false,
      "full_text" : "Here's a secret Tweet.\nNever underestimate the power of IP search engines.\n---\nIni adalah cuitan rahasia.\nJangan pernah meremehkan kekuatan mesin pencarian HAKI. https://t.co/Cc9TXXKSpr",
      "lang" : "in",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1152874363433000960/photo/1",
            "indices" : [
              "162",
              "185"
            ],
            "url" : "https://t.co/Cc9TXXKSpr",
            "media_url" : "http://pbs.twimg.com/media/D__VHLSU4AAuMu7.jpg",
            "id_str" : "1152874348895592448",
            "id" : "1152874348895592448",
            "media_url_https" : "https://pbs.twimg.com/media/D__VHLSU4AAuMu7.jpg",
            "sizes" : {
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/Cc9TXXKSpr"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1151425657156468736"
          ],
          "editableUntil" : "2019-07-17T10:07:23.777Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [ ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Grab Indonesia",
            "screen_name" : "GrabID",
            "indices" : [
              "5",
              "12"
            ],
            "id_str" : "2322081114",
            "id" : "2322081114"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "142"
      ],
      "favorite_count" : "0",
      "id_str" : "1151425657156468736",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1151425657156468736",
      "created_at" : "Wed Jul 17 09:37:23 +0000 2019",
      "favorited" : false,
      "full_text" : "Sore @GrabID, saya mau tanya apakah unit produksi makanan (yang belum punya tempat fisik untuk dijadikan restoran) bisa bergabung ke GrabFood?",
      "lang" : "in"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1144199140953341953"
          ],
          "editableUntil" : "2019-06-27T11:31:48.052Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Vidio",
            "screen_name" : "vidio",
            "indices" : [
              "0",
              "6"
            ],
            "id_str" : "2766004975",
            "id" : "2766004975"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1144199140953341953/photo/1",
            "indices" : [
              "225",
              "248"
            ],
            "url" : "https://t.co/gXhX5Lfi1o",
            "media_url" : "http://pbs.twimg.com/media/D-EDDL1UYAA7EN4.jpg",
            "id_str" : "1144199133579730944",
            "id" : "1144199133579730944",
            "media_url_https" : "https://pbs.twimg.com/media/D-EDDL1UYAA7EN4.jpg",
            "sizes" : {
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              },
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/gXhX5Lfi1o"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "248"
      ],
      "favorite_count" : "0",
      "id_str" : "1144199140953341953",
      "in_reply_to_user_id" : "2766004975",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1144199140953341953",
      "possibly_sensitive" : false,
      "created_at" : "Thu Jun 27 11:01:48 +0000 2019",
      "favorited" : false,
      "full_text" : "@vidio Waktu yang ditampilkan dalam daftar Live Chat di Android masih dalam zona waktu UTC/GMT.\nContoh:\n- Zona Waktu Indonesia Barat itu UTC+7\n- 10.46 AM (Pagi) waktu UTC + 7.00 = 17.46 = 5.46 PM (Sore) Waktu Indonesia Barat https://t.co/gXhX5Lfi1o",
      "lang" : "in",
      "in_reply_to_screen_name" : "vidio",
      "in_reply_to_user_id_str" : "2766004975",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1144199140953341953/photo/1",
            "indices" : [
              "225",
              "248"
            ],
            "url" : "https://t.co/gXhX5Lfi1o",
            "media_url" : "http://pbs.twimg.com/media/D-EDDL1UYAA7EN4.jpg",
            "id_str" : "1144199133579730944",
            "id" : "1144199133579730944",
            "media_url_https" : "https://pbs.twimg.com/media/D-EDDL1UYAA7EN4.jpg",
            "sizes" : {
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              },
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/gXhX5Lfi1o"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1142005438944120833"
          ],
          "editableUntil" : "2019-06-21T10:14:48.736Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "",
            "screen_name" : "ReinPre10",
            "indices" : [
              "3",
              "13"
            ],
            "id_str" : "-1",
            "id" : "-1"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "25",
              "38"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1142005355058102272/photo/1",
            "source_status_id" : "1142005355058102272",
            "indices" : [
              "71",
              "94"
            ],
            "url" : "https://t.co/SmDuwfYKG1",
            "media_url" : "http://pbs.twimg.com/media/D9k3zrXVAAA55SJ.jpg",
            "id_str" : "1142005341468557312",
            "source_user_id" : "3291665198",
            "id" : "1142005341468557312",
            "media_url_https" : "https://pbs.twimg.com/media/D9k3zrXVAAA55SJ.jpg",
            "source_user_id_str" : "3291665198",
            "sizes" : {
              "large" : {
                "w" : "2048",
                "h" : "996",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "1200",
                "h" : "584",
                "resize" : "fit"
              },
              "small" : {
                "w" : "680",
                "h" : "331",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1142005355058102272",
            "display_url" : "pic.twitter.com/SmDuwfYKG1"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "94"
      ],
      "favorite_count" : "0",
      "id_str" : "1142005438944120833",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1142005438944120833",
      "possibly_sensitive" : false,
      "created_at" : "Fri Jun 21 09:44:48 +0000 2019",
      "favorited" : false,
      "full_text" : "RT @ReinPre10: Hey, it's @reinhart1010's first GitLab anniversary! 🎉🎉🎉 https://t.co/SmDuwfYKG1",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/ReinPre10/status/1142005355058102272/photo/1",
            "source_status_id" : "1142005355058102272",
            "indices" : [
              "71",
              "94"
            ],
            "url" : "https://t.co/SmDuwfYKG1",
            "media_url" : "http://pbs.twimg.com/media/D9k3zrXVAAA55SJ.jpg",
            "id_str" : "1142005341468557312",
            "source_user_id" : "3291665198",
            "id" : "1142005341468557312",
            "media_url_https" : "https://pbs.twimg.com/media/D9k3zrXVAAA55SJ.jpg",
            "source_user_id_str" : "3291665198",
            "sizes" : {
              "large" : {
                "w" : "2048",
                "h" : "996",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "1200",
                "h" : "584",
                "resize" : "fit"
              },
              "small" : {
                "w" : "680",
                "h" : "331",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "source_status_id_str" : "1142005355058102272",
            "display_url" : "pic.twitter.com/SmDuwfYKG1"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1141691630379888640"
          ],
          "editableUntil" : "2019-06-20T13:27:50.944Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Gojek Indonesia",
            "screen_name" : "gojekindonesia",
            "indices" : [
              "0",
              "15"
            ],
            "id_str" : "226481275",
            "id" : "226481275"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/BM4K7YsKrh",
            "expanded_url" : "https://twitter.com/gojekindonesia/status/1026442325663342592?s=19",
            "display_url" : "twitter.com/gojekindonesia…",
            "indices" : [
              "39",
              "62"
            ]
          },
          {
            "url" : "https://t.co/WPyDW15GbV",
            "expanded_url" : "https://github.com/webcompat/web-bugs/issues/17876",
            "display_url" : "github.com/webcompat/web-…",
            "indices" : [
              "65",
              "88"
            ]
          }
        ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1141691630379888640/photo/1",
            "indices" : [
              "89",
              "112"
            ],
            "url" : "https://t.co/dYh12fFpTj",
            "media_url" : "http://pbs.twimg.com/media/D9gYQ8cUcAE6vnH.png",
            "id_str" : "1141689184920301569",
            "id" : "1141689184920301569",
            "media_url_https" : "https://pbs.twimg.com/media/D9gYQ8cUcAE6vnH.png",
            "sizes" : {
              "medium" : {
                "w" : "683",
                "h" : "741",
                "resize" : "fit"
              },
              "large" : {
                "w" : "683",
                "h" : "741",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "627",
                "h" : "680",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/dYh12fFpTj"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "112"
      ],
      "favorite_count" : "0",
      "id_str" : "1141691630379888640",
      "in_reply_to_user_id" : "226481275",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1141691630379888640",
      "possibly_sensitive" : false,
      "created_at" : "Thu Jun 20 12:57:50 +0000 2019",
      "favorited" : false,
      "full_text" : "@gojekindonesia 11 bulan kemudian...\n+ https://t.co/BM4K7YsKrh\n+ https://t.co/WPyDW15GbV https://t.co/dYh12fFpTj",
      "lang" : "in",
      "in_reply_to_screen_name" : "gojekindonesia",
      "in_reply_to_user_id_str" : "226481275",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1141691630379888640/photo/1",
            "indices" : [
              "89",
              "112"
            ],
            "url" : "https://t.co/dYh12fFpTj",
            "media_url" : "http://pbs.twimg.com/media/D9gYQ8cUcAE6vnH.png",
            "id_str" : "1141689184920301569",
            "id" : "1141689184920301569",
            "media_url_https" : "https://pbs.twimg.com/media/D9gYQ8cUcAE6vnH.png",
            "sizes" : {
              "medium" : {
                "w" : "683",
                "h" : "741",
                "resize" : "fit"
              },
              "large" : {
                "w" : "683",
                "h" : "741",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "627",
                "h" : "680",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/dYh12fFpTj"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1141680108802940928"
          ],
          "editableUntil" : "2019-06-20T12:42:03.986Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"https://mobile.twitter.com\" rel=\"nofollow\">Twitter Web App</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Kinsta",
            "screen_name" : "kinsta",
            "indices" : [
              "4",
              "11"
            ],
            "id_str" : "2228228652",
            "id" : "2228228652"
          },
          {
            "name" : "webcompat.com",
            "screen_name" : "webcompat",
            "indices" : [
              "146",
              "156"
            ],
            "id_str" : "2244587504",
            "id" : "2244587504"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/3vvePqCdDU",
            "expanded_url" : "https://github.com/webcompat/web-bugs/issues/32499",
            "display_url" : "github.com/webcompat/web-…",
            "indices" : [
              "99",
              "122"
            ]
          }
        ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1141680108802940928/photo/1",
            "indices" : [
              "157",
              "180"
            ],
            "url" : "https://t.co/vyOrGnaUSy",
            "media_url" : "http://pbs.twimg.com/media/D9gQALpUYAAgHEW.png",
            "id_str" : "1141680100850556928",
            "id" : "1141680100850556928",
            "media_url_https" : "https://pbs.twimg.com/media/D9gQALpUYAAgHEW.png",
            "sizes" : {
              "large" : {
                "w" : "436",
                "h" : "717",
                "resize" : "fit"
              },
              "small" : {
                "w" : "414",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "436",
                "h" : "717",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/vyOrGnaUSy"
          }
        ],
        "hashtags" : [ ]
      },
      "display_text_range" : [
        "0",
        "180"
      ],
      "favorite_count" : "0",
      "id_str" : "1141680108802940928",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1141680108802940928",
      "possibly_sensitive" : false,
      "created_at" : "Thu Jun 20 12:12:03 +0000 2019",
      "favorited" : false,
      "full_text" : "Hey @kinsta I think your site has an issue with Firefox for Android (left).\nFor more details visit https://t.co/3vvePqCdDU\nOriginally reported on @webcompat https://t.co/vyOrGnaUSy",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1141680108802940928/photo/1",
            "indices" : [
              "157",
              "180"
            ],
            "url" : "https://t.co/vyOrGnaUSy",
            "media_url" : "http://pbs.twimg.com/media/D9gQALpUYAAgHEW.png",
            "id_str" : "1141680100850556928",
            "id" : "1141680100850556928",
            "media_url_https" : "https://pbs.twimg.com/media/D9gQALpUYAAgHEW.png",
            "sizes" : {
              "large" : {
                "w" : "436",
                "h" : "717",
                "resize" : "fit"
              },
              "small" : {
                "w" : "414",
                "h" : "680",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "436",
                "h" : "717",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/vyOrGnaUSy"
          },
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1141680108802940928/photo/1",
            "indices" : [
              "157",
              "180"
            ],
            "url" : "https://t.co/vyOrGnaUSy",
            "media_url" : "http://pbs.twimg.com/media/D9gQALpUIAAyFXJ.png",
            "id_str" : "1141680100850540544",
            "id" : "1141680100850540544",
            "media_url_https" : "https://pbs.twimg.com/media/D9gQALpUIAAyFXJ.png",
            "sizes" : {
              "large" : {
                "w" : "420",
                "h" : "716",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "420",
                "h" : "716",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "small" : {
                "w" : "399",
                "h" : "680",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/vyOrGnaUSy"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1141282060574658562"
          ],
          "editableUntil" : "2019-06-19T10:20:21.893Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "The Linux Foundation",
            "screen_name" : "linuxfoundation",
            "indices" : [
              "10",
              "26"
            ],
            "id_str" : "14706299",
            "id" : "14706299"
          },
          {
            "name" : "webcompat.com",
            "screen_name" : "webcompat",
            "indices" : [
              "106",
              "116"
            ],
            "id_str" : "2244587504",
            "id" : "2244587504"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/azbJrN6CZq",
            "expanded_url" : "https://webcompat.com/issues/15943",
            "display_url" : "webcompat.com/issues/15943",
            "indices" : [
              "117",
              "140"
            ]
          }
        ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1141282060574658562/photo/1",
            "indices" : [
              "141",
              "164"
            ],
            "url" : "https://t.co/TRSnuvHUvz",
            "media_url" : "http://pbs.twimg.com/media/D9al-ltUwAAo3J0.jpg",
            "id_str" : "1141282050277687296",
            "id" : "1141282050277687296",
            "media_url_https" : "https://pbs.twimg.com/media/D9al-ltUwAAo3J0.jpg",
            "sizes" : {
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/TRSnuvHUvz"
          }
        ],
        "hashtags" : [
          {
            "text" : "firefox",
            "indices" : [
              "62",
              "70"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "164"
      ],
      "favorite_count" : "2",
      "id_str" : "1141282060574658562",
      "truncated" : false,
      "retweet_count" : "1",
      "id" : "1141282060574658562",
      "possibly_sensitive" : false,
      "created_at" : "Wed Jun 19 09:50:21 +0000 2019",
      "favorited" : false,
      "full_text" : "Thank you @linuxfoundation for fixing this \"I am not a robot\" #firefox site issue!\nOriginally reported on @webcompat https://t.co/azbJrN6CZq https://t.co/TRSnuvHUvz",
      "lang" : "en",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1141282060574658562/photo/1",
            "indices" : [
              "141",
              "164"
            ],
            "url" : "https://t.co/TRSnuvHUvz",
            "media_url" : "http://pbs.twimg.com/media/D9al-ltUwAAo3J0.jpg",
            "id_str" : "1141282050277687296",
            "id" : "1141282050277687296",
            "media_url_https" : "https://pbs.twimg.com/media/D9al-ltUwAAo3J0.jpg",
            "sizes" : {
              "small" : {
                "w" : "331",
                "h" : "680",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              },
              "medium" : {
                "w" : "584",
                "h" : "1200",
                "resize" : "fit"
              },
              "large" : {
                "w" : "996",
                "h" : "2048",
                "resize" : "fit"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/TRSnuvHUvz"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1136581768553746434"
          ],
          "editableUntil" : "2019-06-06T11:03:04.948Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "user_mentions" : [
          {
            "name" : "Sean Martell",
            "screen_name" : "mart3ll",
            "indices" : [
              "0",
              "8"
            ],
            "id_str" : "15675252",
            "id" : "15675252"
          },
          {
            "name" : "Firefox 🔥",
            "screen_name" : "firefox",
            "indices" : [
              "9",
              "17"
            ],
            "id_str" : "2142731",
            "id" : "2142731"
          }
        ],
        "urls" : [ ],
        "symbols" : [ ],
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1136581768553746434/photo/1",
            "indices" : [
              "75",
              "98"
            ],
            "url" : "https://t.co/xmcvWg8krJ",
            "media_url" : "http://pbs.twimg.com/media/D8XzFmVUcAA6KnX.jpg",
            "id_str" : "1136581758495780864",
            "id" : "1136581758495780864",
            "media_url_https" : "https://pbs.twimg.com/media/D8XzFmVUcAA6KnX.jpg",
            "sizes" : {
              "small" : {
                "w" : "680",
                "h" : "404",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "1200",
                "h" : "713",
                "resize" : "fit"
              },
              "large" : {
                "w" : "1546",
                "h" : "918",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/xmcvWg8krJ"
          }
        ],
        "hashtags" : [
          {
            "text" : "firedoge",
            "indices" : [
              "23",
              "32"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "98"
      ],
      "favorite_count" : "1",
      "in_reply_to_status_id_str" : "1135908154234028033",
      "id_str" : "1136581768553746434",
      "in_reply_to_user_id" : "15675252",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1136581768553746434",
      "in_reply_to_status_id" : "1135908154234028033",
      "possibly_sensitive" : false,
      "created_at" : "Thu Jun 06 10:33:04 +0000 2019",
      "favorited" : false,
      "full_text" : "@mart3ll @firefox such #firedoge has come back?\nmuch lost in quantum.\nwow. https://t.co/xmcvWg8krJ",
      "lang" : "en",
      "in_reply_to_screen_name" : "mart3ll",
      "in_reply_to_user_id_str" : "15675252",
      "extended_entities" : {
        "media" : [
          {
            "expanded_url" : "https://twitter.com/reinhart1010/status/1136581768553746434/photo/1",
            "indices" : [
              "75",
              "98"
            ],
            "url" : "https://t.co/xmcvWg8krJ",
            "media_url" : "http://pbs.twimg.com/media/D8XzFmVUcAA6KnX.jpg",
            "id_str" : "1136581758495780864",
            "id" : "1136581758495780864",
            "media_url_https" : "https://pbs.twimg.com/media/D8XzFmVUcAA6KnX.jpg",
            "sizes" : {
              "small" : {
                "w" : "680",
                "h" : "404",
                "resize" : "fit"
              },
              "medium" : {
                "w" : "1200",
                "h" : "713",
                "resize" : "fit"
              },
              "large" : {
                "w" : "1546",
                "h" : "918",
                "resize" : "fit"
              },
              "thumb" : {
                "w" : "150",
                "h" : "150",
                "resize" : "crop"
              }
            },
            "type" : "photo",
            "display_url" : "pic.twitter.com/xmcvWg8krJ"
          }
        ]
      }
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1132482144079474688"
          ],
          "editableUntil" : "2019-05-26T03:32:38.306Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [
          {
            "text" : "GitHub",
            "indices" : [
              "67",
              "74"
            ]
          },
          {
            "text" : "GitLab",
            "indices" : [
              "79",
              "86"
            ]
          }
        ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "",
            "screen_name" : "ReinPre10",
            "indices" : [
              "3",
              "13"
            ],
            "id_str" : "-1",
            "id" : "-1"
          },
          {
            "name" : "⬆️ Shift and Shiftine",
            "screen_name" : "reinhart1010",
            "indices" : [
              "27",
              "40"
            ],
            "id_str" : "3291665198",
            "id" : "3291665198"
          }
        ],
        "urls" : [
          {
            "url" : "https://t.co/5TjP1nJwsf",
            "expanded_url" : "https://github.com/reinhart1010/",
            "display_url" : "github.com/reinhart1010/",
            "indices" : [
              "88",
              "111"
            ]
          },
          {
            "url" : "https://t.co/EI5dsGunRh",
            "expanded_url" : "https://gitlab.com/reinhart1010/",
            "display_url" : "gitlab.com/reinhart1010/",
            "indices" : [
              "112",
              "135"
            ]
          }
        ]
      },
      "display_text_range" : [
        "0",
        "140"
      ],
      "favorite_count" : "0",
      "id_str" : "1132482144079474688",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1132482144079474688",
      "possibly_sensitive" : false,
      "created_at" : "Sun May 26 03:02:38 +0000 2019",
      "favorited" : false,
      "full_text" : "RT @ReinPre10: Now you can @reinhart1010 the same way as you do in #GitHub and #GitLab!\nhttps://t.co/5TjP1nJwsf\nhttps://t.co/EI5dsGunRh\nSof…",
      "lang" : "en"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1132325481787809793"
          ],
          "editableUntil" : "2019-05-25T17:10:07.105Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [
          {
            "text" : "NewProfilePic",
            "indices" : [
              "0",
              "14"
            ]
          }
        ],
        "symbols" : [ ],
        "user_mentions" : [ ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "14"
      ],
      "favorite_count" : "0",
      "id_str" : "1132325481787809793",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1132325481787809793",
      "created_at" : "Sat May 25 16:40:07 +0000 2019",
      "favorited" : false,
      "full_text" : "#NewProfilePic",
      "lang" : "qht"
    }
  },
  {
    "tweet" : {
      "edit_info" : {
        "initial" : {
          "editTweetIds" : [
            "1097025204138594304"
          ],
          "editableUntil" : "2019-02-17T07:19:25.066Z",
          "editsRemaining" : "5",
          "isEditEligible" : true
        }
      },
      "retweeted" : false,
      "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "entities" : {
        "hashtags" : [
          {
            "text" : "Ayam",
            "indices" : [
              "166",
              "171"
            ]
          }
        ],
        "symbols" : [ ],
        "user_mentions" : [
          {
            "name" : "Grab Indonesia",
            "screen_name" : "GrabID",
            "indices" : [
              "0",
              "7"
            ],
            "id_str" : "2322081114",
            "id" : "2322081114"
          }
        ],
        "urls" : [ ]
      },
      "display_text_range" : [
        "0",
        "172"
      ],
      "favorite_count" : "0",
      "in_reply_to_status_id_str" : "1096238919941685248",
      "id_str" : "1097025204138594304",
      "in_reply_to_user_id" : "2322081114",
      "truncated" : false,
      "retweet_count" : "0",
      "id" : "1097025204138594304",
      "in_reply_to_status_id" : "1096238919941685248",
      "created_at" : "Sun Feb 17 06:49:25 +0000 2019",
      "favorited" : false,
      "full_text" : "@GrabID Nih ya, kamu lebih suka traktirin pacar ayam GO-RENG atau ayam GrabEk (alias \"geprek\")? Meskipun pilihan kamu beda tapi itu kan artinya kalian sama-sama suka #Ayam.",
      "lang" : "in",
      "in_reply_to_screen_name" : "GrabID",
      "in_reply_to_user_id_str" : "2322081114"
    }
  }
]
